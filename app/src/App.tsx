import React from 'react';
import './App.css';
import { Sidebar } from './Sidebar';

function App() {
  return (
    <div className="app">
      <h1>Biscord</h1>
      { /** Sidebar */}
      <Sidebar/>
      { /** Chat */}
    </div>
  );
}

export default App;
